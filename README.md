# brain_vc

- `brain_idl` version: 1.0.0
- 관련 컨플루언스 글: https://pms.maum.ai/confluence/x/C2KqAQ

Pipelining `brain_cotatron` and `brain_melgan` for Voice Conversion (VC).

## Build
```bash
docker build -f Dockerfile -t brain_vc:<version> .
```

## Run

```bash
docker run -p <port>:34001 \
-e COTATRON_REMOTE=<url:port> \
-e VOCODER_REMOTE=<url:port> \
brain_vc:<version>
```

## Author

브레인 박승원 수석
