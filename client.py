import grpc
import argparse
import google.protobuf.empty_pb2 as empty

from vc_pb2 import WavTxtTargetspk
from vc_pb2_grpc import VoiceConversionStub


class VoiceConversionClient(object):
    def __init__(self, remote='127.0.0.1:34001', chunk_size=3145728):
        channel = grpc.insecure_channel(remote)
        self.stub = VoiceConversionStub(channel)
        self.chunk_size = chunk_size

    def wavtxt2wav(self, wav_binary, text, speaker):
        message = self._generate_wav_binary_iterator(wav_binary, text, speaker)
        return self.stub.WavTxtTargetspk2Wav(message)

    def _generate_wav_binary_iterator(self, wav_binary, text, speaker):
        for idx in range(0, len(wav_binary), self.chunk_size):
            yield WavTxtTargetspk(
                wav_source=wav_binary[idx:idx+self.chunk_size], text=text, spk_target=speaker)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Voice Conversion Client')
    parser.add_argument('-r', '--remote', default='127.0.0.1:34001',
                        help="grpc: ip:port")
    parser.add_argument('-i', '--input', type=str, required=True,
                        help="input wav file")
    parser.add_argument('-t', '--text', type=str, required=True,
                        help="input text")
    parser.add_argument('-s', '--speaker', type=int, required=True,
                        help="target speaker index")
    parser.add_argument('-o', '--output', type=str, required=True,
                        help="output wav file")
    args = parser.parse_args()

    client = VoiceConversionClient(args.remote)

    with open(args.input, 'rb') as rf:
        wav_binary = rf.read()

    result = client.wavtxt2wav(wav_binary, args.text, args.speaker)
    with open(args.output, 'wb') as wf:
        for data in result:
            wf.write(data.data)
