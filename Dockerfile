FROM nvcr.io/nvidia/pytorch:19.06-py3
RUN python3 -m pip --no-cache-dir install --upgrade \
        omegaconf==2.0.0 \
        grpcio==1.13.0 \
        grpcio-tools==1.13.0 \
        protobuf==3.6.0 \
        && \
mkdir /root/vc
COPY . /root/vc
RUN cd /root/vc/ && \
    python3 -m grpc.tools.protoc --proto_path=brain_idl/protos/audio/legacy --python_out=. --grpc_python_out=. vc.proto && \
    python3 -m grpc.tools.protoc --proto_path=brain_idl/protos/audio/legacy --python_out=. --grpc_python_out=. cotatron.proto && \
    python3 -m grpc.tools.protoc --proto_path=brain_idl/protos/audio/legacy --python_out=. --grpc_python_out=. tts.proto
EXPOSE 34001
WORKDIR /root/vc/
ENTRYPOINT python server.py --cotatron_remote $COTATRON_REMOTE --vocoder_remote $VOCODER_REMOTE
