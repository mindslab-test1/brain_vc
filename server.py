import time
import grpc
import logging
import argparse
import numpy as np

from concurrent import futures
from vc_pb2 import WavData
from vc_pb2_grpc import add_VoiceConversionServicer_to_server, VoiceConversionServicer
from cotatron_pb2_grpc import MelConversionStub
from tts_pb2_grpc import VocoderStub
from wav_maker import WavMaker

_ONE_DAY_IN_SECONDS = 60 * 60 * 24


class VoiceConversionImpl(VoiceConversionServicer):
    def __init__(self, cotatron_remote, vocoder_remote, sample_rate=22050,
                 mel_chunk_size=88, hop_length=256, crossfade_size=200):
        self.cotatron_remote = cotatron_remote
        self.vocoder_remote = vocoder_remote
        self.wav_maker = WavMaker(sample_rate)
        self.mel_chunk_size = mel_chunk_size
        self.hop_length = hop_length
        self.crossfade_size = crossfade_size

    def _make_mel_generator(self, mel, mel_generator):
        yield mel
        yield from mel_generator

    def WavTxtTargetspk2Wav(self, message, context):
        try:
            with grpc.insecure_channel(self.cotatron_remote) as cotatron_channel, \
                grpc.insecure_channel(self.vocoder_remote) as vocoder_channel:
                # cotatron
                cotatron_stub = MelConversionStub(cotatron_channel)
                mel_generator = cotatron_stub.WavTxtTargetspk2Mel(message)
                mel = next(mel_generator)
                yield WavData(data=self.wav_maker.make_header(mel.sample_length))
                mel_generator = self._make_mel_generator(mel, mel_generator)
                # vocoder
                vocoder_stub = VocoderStub(vocoder_channel)
                for wav in vocoder_stub.StreamMel2Wav(mel_generator):
                    wav = np.array(wav.data, dtype=self.wav_maker.dtype)
                    wav = self.wav_maker.make_data(wav)
                    yield WavData(data=wav.tobytes())

        except Exception as e:
            logging.exception(e)
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Server for pipelining Cotatron + Vocoder')
    parser.add_argument('-p', '--port', type=int, default=34001)
    parser.add_argument('-l', '--log_level', type=str, default='INFO')
    parser.add_argument('--cotatron_remote', type=str, default='127.0.0.1:30002')
    parser.add_argument('--vocoder_remote', type=str, default='127.0.0.1:35002')
    args = parser.parse_args()

    servicer = VoiceConversionImpl(args.cotatron_remote, args.vocoder_remote)

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1), )
    add_VoiceConversionServicer_to_server(servicer, server)
    server.add_insecure_port('[::]:{}'.format(args.port))
    server.start()

    logging.basicConfig(
        level=getattr(logging, args.log_level),
        format='[%(levelname)s|%(filename)s:%(lineno)s][%(asctime)s] >>> %(message)s'
    )
    logging.info('Voice Conversion (Cotatron+Vocoder pipeline) starting at 0.0.0.0:%d', args.port)

    try:
        while True:
            # Sleep forever, since `start` doesn't block
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)
